package com.dummy.myerp.model.bean.comptabilite;

import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;

import java.text.MessageFormat;
import java.time.Duration;
import java.time.Instant;

@Tag("SequenceEcritureComptableTest")
@DisplayName("Réussir a verifier une sequence ecriture comptable.")
@ExtendWith(LoggingExtension.class)
public class SequenceEcritureComptableTest {

    private SequenceEcritureComptable sequenceEcritureComptable;

    private static Instant startedAt;

    private Logger logger;

    public void setLogger(Logger logger) {
        this.logger = logger;
    }


    @BeforeEach
    public void initSequenceEcritureComptable() {
        logger.info("Appel avant chaque test");
        sequenceEcritureComptable = new SequenceEcritureComptable();
    }

    @AfterEach
    public void undefSequenceEcritureComptable () {
        logger.info("Appel après chaque test");
        sequenceEcritureComptable = null;
    }

    @BeforeAll
    public static void initStartingTime() {
        System.out.println("Appel avant tous les tests");
        startedAt = Instant.now();
    }

    @AfterAll
    public static void showTestDuration() {
        System.out.println("Appel après tous les tests");
        final Instant endedAt = Instant.now();
        final long duration = Duration.between(startedAt, endedAt).toMillis();
        System.out.println(MessageFormat.format("Durée des tests : {0} ms", duration));
    }



    @Test
    public void testSequenceEcritureComptableTest() {

        // GIVEN
        final int dernierValeur = 2;
        final int annee = 1994;
        final String code = "BQ";


        // WHEN
        SequenceEcritureComptable sequenceEcritureComptable = new SequenceEcritureComptable();
        sequenceEcritureComptable.setAnnee(annee);
        sequenceEcritureComptable.setDerniereValeur(dernierValeur);
        sequenceEcritureComptable.setCode(code);
        sequenceEcritureComptable.getAnnee();
        sequenceEcritureComptable.getDerniereValeur();
        sequenceEcritureComptable.getCode();
        sequenceEcritureComptable.toString();
    }


    @org.junit.Test
    public void TestSequenceECConstructeur(){

        SequenceEcritureComptable sequenceEcritureComptable = new SequenceEcritureComptable(2019, 1234);
        sequenceEcritureComptable.toString();
        SequenceEcritureComptable sequenceEcritureComptable1 = new SequenceEcritureComptable(2019, 1234, "ABCD");

    }
}
