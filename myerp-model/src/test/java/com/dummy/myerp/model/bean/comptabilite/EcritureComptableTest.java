package com.dummy.myerp.model.bean.comptabilite;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;

@Tag("EcritureComptableTest")
@DisplayName("Réussir a faire verifier une ecriture comptable.")
@ExtendWith(LoggingExtension.class)
public class EcritureComptableTest {

    EcritureComptable ecritureComptable = new EcritureComptable();

    private static Instant startedAt;

    private Logger logger;

    public void setLogger(Logger logger) {
        this.logger = logger;
    }


    @BeforeEach
    public void initLigneEcritureComptable () {
        logger.info("Appel avant chaque test");
        ecritureComptable = new EcritureComptable();
    }

    @AfterEach
    public void undefLigneEcritureComptable() {
        logger.info("Appel après chaque test");
        ecritureComptable = null;
    }

    @BeforeAll
    public static void initStartingTime() {
        System.out.println("Appel avant tous les tests");
        startedAt = Instant.now();
    }

    @AfterAll
    public static void showTestDuration() {
        System.out.println("Appel après tous les tests");
        final Instant endedAt = Instant.now();
        final long duration = Duration.between(startedAt, endedAt).toMillis();
        System.out.println(MessageFormat.format("Durée des tests : {0} ms", duration));
    }


    private LigneEcritureComptable createLigne(Integer pCompteComptableNumero, String pDebit, String pCredit) {
        BigDecimal vDebit = pDebit == null ? null : new BigDecimal(pDebit);
        BigDecimal vCredit = pCredit == null ? null : new BigDecimal(pCredit);
        String vLibelle = ObjectUtils.defaultIfNull(vDebit, BigDecimal.ZERO)
                                     .subtract(ObjectUtils.defaultIfNull(vCredit, BigDecimal.ZERO)).toPlainString();
        LigneEcritureComptable vRetour = new LigneEcritureComptable(new CompteComptable(pCompteComptableNumero),
                                                                    vLibelle,
                                                                    vDebit, vCredit);
        return vRetour;
    }

    @Test
    public void isEquilibree() {
        EcritureComptable vEcriture;
        vEcriture = new EcritureComptable();

        vEcriture.setLibelle("Equilibrée");
        vEcriture.getListLigneEcriture().add(this.createLigne(1, "200.50", null));
        vEcriture.getListLigneEcriture().add(this.createLigne(1, "100.50", "33"));
        vEcriture.getListLigneEcriture().add(this.createLigne(2, null, "301"));
        vEcriture.getListLigneEcriture().add(this.createLigne(2, "40", "7"));
        Assert.assertTrue(vEcriture.toString(), vEcriture.isEquilibree());
        System.out.println(vEcriture.toString());
        System.out.println(vEcriture.isEquilibree());

        vEcriture.getListLigneEcriture().clear();
        vEcriture.setLibelle("Non équilibrée");
        vEcriture.getListLigneEcriture().add(this.createLigne(1, "10", null));
        vEcriture.getListLigneEcriture().add(this.createLigne(1, "20", "1"));
        vEcriture.getListLigneEcriture().add(this.createLigne(2, null, "30"));
        vEcriture.getListLigneEcriture().add(this.createLigne(2, "1", "2"));
        Assert.assertFalse(vEcriture.toString(), vEcriture.isEquilibree());
        System.out.println(vEcriture.toString());
        System.out.println(vEcriture.isEquilibree());
    }


    @Test
    public void getTotalDebit() {
        EcritureComptable vEcriture;
        vEcriture = new EcritureComptable();
        vEcriture.setLibelle("Test Total Débit ");
        vEcriture.getListLigneEcriture().add(this.createLigne(1, "3", null));
        vEcriture.getListLigneEcriture().add(this.createLigne(1, "-2", "1"));
        vEcriture.getListLigneEcriture().add(this.createLigne(2, "2", "1.7"));
        vEcriture.getListLigneEcriture().add(this.createLigne(2, "5", "4"));
        Assert.assertEquals(vEcriture.getTotalDebit(), new BigDecimal("8" ));
        System.out.println(vEcriture.getTotalDebit());


        // test si aucun montant au débit :

        EcritureComptable vEcritureNull;
        vEcritureNull = new EcritureComptable();
        vEcritureNull.setLibelle("Test Total Débit ");
        vEcritureNull.getListLigneEcriture().add(this.createLigne(1, null, "5.23"));
        vEcritureNull.getListLigneEcriture().add(this.createLigne(1, null, "2"));
        vEcritureNull.getListLigneEcriture().add(this.createLigne(2, null, "3.5"));
        vEcritureNull.getListLigneEcriture().add(this.createLigne(2, null, "5"));
        Assert.assertEquals(vEcritureNull.getTotalDebit(), new BigDecimal("0" ));
        System.out.println(vEcritureNull.getTotalDebit());


    }

    @Test
    public void getTotalCredit() {

        EcritureComptable vEcriture;
        vEcriture = new EcritureComptable();
        vEcriture.setLibelle("Test credit total");
        vEcriture.getListLigneEcriture().add(this.createLigne(1, "15.15", null));
        vEcriture.getListLigneEcriture().add(this.createLigne(1, "16.16", "17"));
        vEcriture.getListLigneEcriture().add(this.createLigne(2, null, "23.36"));
        vEcriture.getListLigneEcriture().add(this.createLigne(2, "4", "26.3"));
        Assert.assertEquals(vEcriture.getTotalCredit(), new BigDecimal("66.66" ));
        System.out.println(vEcriture.getTotalCredit());
        // test si aucun montant au débit :


        EcritureComptable vEcritureNull;
        vEcritureNull = new EcritureComptable();
        vEcritureNull.setLibelle("Test Total Débit ");
        vEcritureNull.getListLigneEcriture().add(this.createLigne(1, "6", null));
        vEcritureNull.getListLigneEcriture().add(this.createLigne(1, "666.66", null));
        vEcritureNull.getListLigneEcriture().add(this.createLigne(2, "666.66", null));
        vEcritureNull.getListLigneEcriture().add(this.createLigne(2, "66", null));
        Assert.assertEquals(vEcritureNull.getTotalCredit(), new BigDecimal("0" ));
        System.out.println(vEcritureNull.getTotalCredit());

    }

    @Test
    public void testEcritureComptable(){
        EcritureComptable vEcriture = new EcritureComptable();
        vEcriture.setLibelle("Libellé de mon écriture");
        vEcriture.setReference("Test de référence");
        vEcriture.setDate(new Date());
        vEcriture.setId(99999);
        vEcriture.setJournal(new JournalComptable("ABC", "Libellé de journal"));
        vEcriture.toString();
        vEcriture.getId();
        vEcriture.getJournal();
        vEcriture.getReference();
        vEcriture.getDate();
        vEcriture.getLibelle();

    }

    @Test
    public void toStringTest(){
        EcritureComptable vEcriture = new EcritureComptable();
        vEcriture.toString();
    }

    }
