package com.dummy.myerp.model.bean.comptabilite;

import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;

import java.text.MessageFormat;
import java.time.Duration;
import java.time.Instant;

@Tag("CompteComptableTest")
@DisplayName("Réussir a faire verifier un compte comptable.")
@ExtendWith(LoggingExtension.class)
public class CompteComptableTest {


    private  CompteComptable compteComptable;

    private static Instant startedAt;

    private Logger logger;

    public void setLogger(Logger logger) {
        this.logger = logger;
    }


    @BeforeEach
    public void initEcritureComptable() {
        logger.info("Appel avant chaque test");
        compteComptable = new CompteComptable();
    }

    @AfterEach
    public void undefEcritureComptable() {
        logger.info("Appel après chaque test");
        compteComptable = null;
    }

    @BeforeAll
    public static void initStartingTime() {
        System.out.println("Appel avant tous les tests");
        startedAt = Instant.now();
    }

    @AfterAll
    public static void showTestDuration() {
        System.out.println("Appel après tous les tests");
        final Instant endedAt = Instant.now();
        final long duration = Duration.between(startedAt, endedAt).toMillis();
        System.out.println(MessageFormat.format("Durée des tests : {0} ms", duration));
    }


    @Test
    public void testCompteComptable() {

        // GIVEN
        final String libelle = "Test 1";
        final int numero = 7;

        // WHEN
        CompteComptable compteComptable = new CompteComptable();
        compteComptable.setNumero(numero);
        compteComptable.setLibelle(libelle);
        compteComptable.getLibelle();
        compteComptable.getNumero();
        compteComptable.toString();
    }

    // test du constructeur avec param
    @Test
    public void testCompteComptableBis() {
        CompteComptable compteComptable = new CompteComptable(111,"Test de libellé");
        compteComptable.toString();
    }
}
