package com.dummy.myerp.model.bean.comptabilite;

import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;

import java.text.MessageFormat;
import java.time.Duration;
import java.time.Instant;

@Tag("CompteComptableTest")
@DisplayName("Réussir a verifier un journal comptable.")
@ExtendWith(LoggingExtension.class)
public class JournalComptableTest {

    private  JournalComptable journalComptable;

    private static Instant startedAt;

    private Logger logger;

    public void setLogger(Logger logger) {
        this.logger = logger;
    }


    @BeforeEach
    public void initJournalComptable() {
        logger.info("Appel avant chaque test");
        journalComptable = new JournalComptable();
    }

    @AfterEach
    public void undefJournalComptable() {
        logger.info("Appel après chaque test");
        journalComptable = null;
    }

    @BeforeAll
    public static void initStartingTime() {
        System.out.println("Appel avant tous les tests");
        startedAt = Instant.now();
    }

    @AfterAll
    public static void showTestDuration() {
        System.out.println("Appel après tous les tests");
        final Instant endedAt = Instant.now();
        final long duration = Duration.between(startedAt, endedAt).toMillis();
        System.out.println(MessageFormat.format("Durée des tests : {0} ms", duration));
    }


    @Test
    public void testJournalComptable() {

        JournalComptable vJournal = new JournalComptable();
        vJournal.setCode("AC");
        vJournal.setLibelle("libelle");
        vJournal.getCode();
        vJournal.getLibelle();
        vJournal.toString();
    }


    @Test
    public void testJournalComptableBis() {

        JournalComptable journalComptable = new JournalComptable("AC", "libelle");
        journalComptable.toString();

    }
}
